# Rest Password Reset

#### Introduction

The Rest Password Reset module enhances Drupal 10+ functionality by providing REST API endpoints to request a username via email and change a user's password. This is particularly useful for decoupled websites with a React frontend, where users can retrieve their username or reset their password through the provided endpoints.

## Features

The module offers three REST endpoints:

1. **GET /api/user/username/{email address}**
2. **GET /api/user/password/{email address}**
3. **POST /api/user/password/reset**

The second endpoint targets the decoupled frontend, allowing users to provide a new password through a frontend page. The parameters provided in the URL are then posted to the backend via the third endpoint.

## Post-Installation

1. Access the multilingual form under Admin->Configuration->Web Services->Rest Password Reset.
2. Use the REST UI module to configure and activate the custom endpoints.
3. Ensure anonymous users have access to these endpoints with Cookie authentication.

## Additional Requirements

- This module extends the core REST module and is designed exclusively for decoupled websites.
- A React frontend is a mandatory requirement.

## Recommended Modules/Libraries

- **rest** (Drupal core)
- **rest ui** (3rd party Drupal module)

## Usage

1. For username retrieval, make a GET request to `/api/user/username/{email address}`.
2. For password reset, make a GET request to `/api/user/password/{email address}`. This sends an email to the frontend with a link for password reset, e.g., `/password-reset/{uid}/{timestamp}/{hash code}`. It is possible to provide a custom uri suffix in the config form, however {uid}, {timestamp} and {hash code} are always suffixed on top of that as url parameters.
3. The frontend should provide a dialog where users can set a new password and make a POST request to `/api/user/password/reset` with the following JSON body:

```json
{
    "uid": {uid},
    "timestamp": {timestamp},
    "hash": {hash},
    "new_password": {password}
}
```
The Drupal backend will respond with success or error indications in the JSON response.

## Multilingual Support

The module fully supports multilingual functionality.


