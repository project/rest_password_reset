<?php

/**
 * @file
 * Token callbacks for the rest_password_reset module.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;

/**
 * Implements hook_token_info().
 */
function rest_password_reset_token_info() {
  $types['rest_password_reset'] = [
    'name' => t('Rest password reset'),
    'description' => t('Rest password reset tokens related to individual user accounts.'),
    'needs-data' => 'user',
  ];

  $rest_password_reset['login-link'] = [
    'name' => t('One-time login URL'),
    'description' => t('This is a one-time time based login URL for the provided user account.'),
  ];

  return [
    'types' => $types,
    'tokens' => ['rest_password_reset' => $rest_password_reset],
  ];
}

/**
 * Implements hook_tokens().
 */
function rest_password_reset_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $config = \Drupal::config('rest_password_reset.password_reset');
  if (isset($options['langcode'])) {
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }
  $replacements = [];

  if ($type == 'rest_password_reset' && !empty($data['user'])) {
    /** @var \Drupal\user\UserInterface $account */
    $account = $data['user'];
    foreach ($tokens as $name => $original) {
      // Perhaps more tokens are added later.
      switch ($name) {
        // One-time login-link.
        case 'login_link':
          $timestamp = \Drupal::time()->getRequestTime();
          $hash = user_pass_rehash($account, $timestamp);
          $bubbleable_metadata->setCacheMaxAge(300);
          $replacements[$original] = Url::fromUri(
            $config->get('fe_uri') .
            ($langcode ? '/' . $langcode : '') .
            (
              $config->get('fe_uri_custom') &&
              !empty($config->get('fe_uri_suffix')) ?
                $config->get('fe_uri_suffix') :
                '/password-reset/'
            ) .
            $account->id() .
            '/' . $timestamp .
            '/' . $hash
          )->toString();

          break;
      }
    }
  }

  return $replacements;
}
