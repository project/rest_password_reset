<?php

namespace Drupal\rest_password_reset\Plugin\rest\resource;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Utility\Token;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to request an reset link email.
 *
 * @RestResource(
 *   id = "rest_password_reset_link",
 *   label = @Translation("Rest password reset link request"),
 *   uri_paths = {
 *     "canonical" = "/api/user/reset/{mail}"
 *   }
 * )
 */
class ResetLink extends ResourceBase {

  /**
   * Constructs a new ResetLink object.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param array $serializer_formats
   *   The serializer formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $emailValidator
   *   The email validator service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected Token $token,
    protected ConfigFactoryInterface $configFactory,
    protected EmailValidatorInterface $emailValidator,
    protected MailManagerInterface $mailManager,
    protected LanguageManagerInterface $languageManager,
    protected StateInterface $state,
    protected TimeInterface $time
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('config.factory'),
      $container->get('email.validator'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('state'),
      $container->get('datetime.time')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $mail
   *   The user email address to lookup.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   A message on succes or error condition.
   */
  public function get(string $mail): ModifiedResourceResponse {
    $response = new ModifiedResourceResponse(['message' => $this->t('If there is an active user, an email with the reset link was sent.')], 200);
    $valid_request = TRUE;
    if (!$this->emailValidator->isValid($mail)) {
      $this->logger->warning('An invalid email address was used on the rest_password_reset_link REST resource.');
      $response = new ModifiedResourceResponse(['message' => $this->t('Please provide a valid email address.')], 400);
      $valid_request = FALSE;
    }
    if ($valid_request) {
      $users = $this->entityTypeManager->getStorage('user')
        ->loadByProperties(['mail' => $mail]);
      if ($users) {
        /** @var \Drupal\user\UserInterface $user */
        $user = reset($users);
        $config = $this->configFactory->get('rest_password_reset.password_reset');
        $langcode = $this->languageManager->getCurrentLanguage()->getId();
        $token_options = ['langcode' => $langcode];
        $last = $this->state->get('rest_password_reset.last_link.' . $user->id());
        $request_timestamp = $this->time->getRequestTime();
        if ($last > 0 && ($request_timestamp - $last) < 300) {
          $this->logger->warning('A one-time login link for user :user was requested within 5 minutes.', [':user' => $user->getAccountName()]);
          $valid_request = FALSE;
        }

        if ($valid_request) {
          $mail = $this->mailManager->mail(
            'rest_password_reset',
            'reset_mail',
            $user->getEmail(),
            $langcode,
            [
              'subject' => $this->token->replace($config->get('reset_mail_subject'), ['user' => $user], $token_options),
              'body' => $this->token->replace($config->get('reset_mail_body'), ['user' => $user], $token_options),
            ],
          );

          if ($mail['send'] && !$mail['result']) {
            $response = new ModifiedResourceResponse(['message' => $this->t('Could not send email, please try again.')], 500);
          }
          else {
            $this->state->set('rest_password_reset.last_link.' . $user->id(), $this->time->getCurrentTime());
          }
        }
      }
    }

    $response->setMaxAge(0);

    return $response;
  }

}
