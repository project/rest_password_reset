<?php

namespace Drupal\rest_password_reset\Plugin\rest\resource;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to change a user's password.
 *
 * @RestResource(
 *   id = "rest_password_reset_password",
 *   label = @Translation("Rest password reset"),
 *   uri_paths = {
 *     "create" = "/api/user/reset/password"
 *   }
 * )
 */
class PasswordReset extends ResourceBase {

  /**
   * Constructs a new PasswordReset object.
   *
   * @param array $configuration
   *   The configuration array.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition array.
   * @param array $serializer_formats
   *   The serializer formats array.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ConfigFactoryInterface $configFactory,
    protected TimeInterface $time
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The POST request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   */
  public function post(Request $request): ResourceResponse {
    $params = Json::decode($request->getContent());
    if (
      !array_key_exists('uid', $params) ||
      !is_numeric($params['uid']) ||
      !array_key_exists('timestamp', $params) ||
      !is_numeric($params['timestamp']) ||
      !array_key_exists('hash', $params) ||
      !is_string($params['hash']) ||
      !array_key_exists('new_password', $params) ||
      !is_string($params['new_password'])
    ) {
      $this->logger->warning('REST resource rest_password_reset_password: invalid parameter set send.');
      return new ResourceResponse(['message' => $this->t('Invalid parameter set send!')], 500);
    }

    try {
      /** @var \Drupal\user\UserInterface|NULL $user */
      $user = $this->entityTypeManager->getStorage('user')
        ->load($params['uid']);
      if (!$user) {
        $this->logger->warning('REST resource rest_password_reset_password: unable to load user by id.');
        return new ResourceResponse(['message' => $this->t('Unable to load user.')], 500);
      }
    }
    catch (PluginNotFoundException | InvalidPluginDefinitionException $e) {
      $this->logger->error('Internal server error: :error', [':error' => $e]);
      return new ResourceResponse(['message' => $this->t('Internal server error: :error', [':error' => $e])], 500);
    }

    $config = $this->configFactory->get('user.settings');
    $timeout = $config->get('password_reset_timeout');
    $current = $this->time->getRequestTime();
    if ($current - $params['timestamp'] > $timeout) {
      return new ResourceResponse(['message' => $this->t('Your one-time login link has expired.')], 404);
    }

    if (!hash_equals($params['hash'], user_pass_rehash($user, $params['timestamp']))) {
      return new ResourceResponse(['message' => $this->t('Invalid reset code.')], 404);
    }

    $user->setPassword($params['new_password']);
    try {
      $user->save();
    }
    catch (EntityStorageException $e) {
      $this->logger->error('Internal server error: :error', [':error' => $e]);
      return new ResourceResponse(['message' => $this->t('Internal server error: :error', [':error' => $e])], 500);
    }

    return new ResourceResponse(['message' => $this->t('Your password has been reset, you can now login with your new password.')], 200);
  }

}
