<?php

namespace Drupal\rest_password_reset\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The rest_password_reset configuration form.
 */
class PasswordResetConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rest_password_reset_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rest_password_reset.password_reset'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $password_reset_config = $this->config('rest_password_reset.password_reset');

    $form['rest_password_reset'] = [
      '#type' => 'details',
      '#title' => $this->t('Rest API password reset details'),
      '#open' => TRUE,
    ];
    $form['rest_password_reset']['fe_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Frontend uri'),
      '#default_value' => $password_reset_config->get('fe_uri'),
      '#required' => TRUE,
    ];
    $form['rest_password_reset']['fe_uri_custom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Frontend uri: custom suffix'),
      '#default_value' => $password_reset_config->get('fe_uri_custom'),
      '#required' => FALSE,
    ];
    $form['rest_password_reset']['fe_uri_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Frontend uri: custom suffix'),
      '#default_value' => Xss::filter($password_reset_config->get('fe_uri_suffix')),
      '#description' => $this->t('Provide your own endpoint here, endpoint is prefixed by langcode when necessary.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="fe_uri_custom"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['rest_password_reset']['name_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username retrieve email subject'),
      '#default_value' => Xss::filter($password_reset_config->get('name_mail_subject')),
      '#description' => $this->t('The subject used in the email send after a username request.'),
      '#required' => TRUE,
      '#maxlength' => 255,
    ];
    $form['rest_password_reset']['name_mail_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Username retrieve email body'),
      '#default_value' => Xss::filter($password_reset_config->get('name_mail_body')),
      '#description' => $this->t('The email send after a username request.'),
      '#required' => TRUE,
      '#rows' => 20,
    ];
    $form['rest_password_reset']['reset_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password reset email subject'),
      '#default_value' => Xss::filter($password_reset_config->get('reset_mail_subject')),
      '#description' => $this->t('The subject used in the email send after a password reset request.'),
      '#required' => TRUE,
      '#maxlength' => 255,
    ];
    $form['rest_password_reset']['reset_mail_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Password reset email body'),
      '#default_value' => Xss::filter($password_reset_config->get('reset_mail_body')),
      '#description' => $this->t('The email send after a password reset request.'),
      '#required' => TRUE,
      '#rows' => 20,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('rest_password_reset.password_reset')
      ->set('fe_uri', $form_state->getValue('fe_uri'))
      ->set('fe_uri_custom', $form_state->getValue('fe_uri_custom'))
      ->set('fe_uri_suffix', $form_state->getValue('fe_uri_suffix'))
      ->set('name_mail_subject', $form_state->getValue('name_mail_subject'))
      ->set('name_mail_body', $form_state->getValue('name_mail_body'))
      ->set('reset_mail_subject', $form_state->getValue('reset_mail_subject'))
      ->set('reset_mail_body', $form_state->getValue('reset_mail_body'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
